language = "FRENCH"
greeting = ""

if language.lower() == "tagalog":
    greeting = "Kamusta"
elif language.lower() == "english":
    greeting = "Hello"
elif language.lower() == "german":
    greeting = "Hallo"
elif language.lower() == "french":
    greeting = "Bonjour"
else:
    greeting = "I don't know how to speak that language"

print(greeting)
