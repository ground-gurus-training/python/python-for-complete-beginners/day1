message = "the big red fox jumps over the lazy dog"

print(message.lower())  # returns the string as lowercase
print(message.upper())  # returns the string as uppercase
print(message.count("lazy"))  # counts the number of instances of the passed string
print(message.index("red"))  # returns the index of the passed string
print("#".join(["Visit", "us", "at", "groundgurus.com"]))  # joins an iterable string
print(message.replace("fox", "dog"))  # replaces the first string with the second string
